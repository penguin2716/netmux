#!/usr/bin/env ruby
# coding: utf-8

BLACKLIST = ["lo", "eth0", "tun0"]
$logfile = nil

Interface = Struct.new(:ifname, :address, :network, :gateway, :prefix)

def puts_message(str = nil)
  if $logfile
    $logfile.puts str
    $logfile.flush
  else
    puts str
  end
end

def print_message(str = nil)
  if $logfile
    $logfile.print str
    $logfile.flush
  else
    print str
  end
end

def inet2net(src)
  inet_str10, prefix = src.split("/")
  inet_str2 = inet_str10.split(".").map{|s| "%08d" % s.to_i.to_s(2)}.join
  net_str2 = inet_str2[0..prefix.to_i-1] + "0" * (32 - prefix.to_i)
  net_str10 = net_str2.split("").each_slice(8).to_a.map{|a| a.join.to_i(2)}.join(".")
  net_str10 + "/" + prefix
end

def suppose_gateway(inet_str10)
  net_str10 = inet2net(inet_str10)
  return "172.20.10.1" if net_str10 =~ /172\.20\.10\.[0-9]+\/28/ # iPhone
  return "192.168.42.129" if net_str10 =~ /192\.168\.42\.[0-9]+\/24/ # Android
  return nil if net_str10 =~ /169\.254\.[0-9]+\.[0-9]+\/16/ # Android
  print_message "\033[33;1m>>\033[0m Enter gateway of (#{inet_str10}): "
  gateway = STDIN.gets.chomp
  gateway
end

def get_interfaces(blacklist = [])
  interfaces = []
  ifaces = `ip -4 a | grep '<' | awk -F ': ' '{print $2}'`.split("\n")
  ifaces.each do |iface|
    next if blacklist.include?(iface)
    inet = `ip -4 a show dev #{iface} | grep inet | awk '{print $2}'`.chomp
    iface_obj = Interface.new(
      iface,
      inet.split("/").first,
      inet2net(inet).split("/").first,
      suppose_gateway(inet),
      inet.split("/").last.to_i
    )
    interfaces << iface_obj if iface_obj.gateway
  end
  interfaces
end

def add_routing_rules(iface, table_id)
  commands = []
  commands << "ip rule add from #{iface.address} oif #{iface.ifname} table #{table_id}"
  commands << "ip route add #{iface.network}/#{iface.prefix} dev #{iface.ifname} scope link table #{table_id}"
  commands << "ip route add default via #{iface.gateway} dev #{iface.ifname} table #{table_id}"
  puts_message "\033[32;1m*\033[0m Adding new routing rules for #{iface.ifname}..." unless commands.empty?
  commands.each do |cmd|
    puts_message "  - #{cmd}"
    `#{cmd}`
  end
end

def del_routing_rules
  commands = []
  table_ids = `ip rule | grep -v all | awk '{print $NF}'`.split("\n")
  table_ids.each do |table_id|
    rules = `ip route show table #{table_id}`.split("\n")
    rules.each do |rule|
      commands << "ip route del #{rule} table #{table_id}"
    end
    commands << "ip rule del table #{table_id}"
  end
  puts_message "\033[32;1m*\033[0m Deleting existing routing rules..." unless commands.empty?
  commands.each do |cmd|
    puts_message "  - #{cmd}"
    `#{cmd}`
  end
end

def setup_mptcp(num_subflows)
  commands = []
	commands << "echo 'nameserver 8.8.4.4' > /etc/resolv.conf"
	commands << "echo 1 > /proc/sys/net/mptcp/mptcp_enabled"
	commands << "echo fullmesh > /proc/sys/net/mptcp/mptcp_path_manager"
	commands << "echo #{num_subflows} > /sys/module/mptcp_fullmesh/parameters/num_subflows"
	commands << "echo cubic > /proc/sys/net/ipv4/tcp_congestion_control"
  puts_message "\033[32;1m*\033[0m Setting MPTCP parameters..." unless commands.empty?
  commands.each do |cmd|
    puts_message "  - #{cmd}"
    `#{cmd}`
  end
end

def set_firewall_rules(gateway_ifname)
  commands = []
  commands << "echo 1 > /proc/sys/net/ipv4/ip_forward"
  commands << "iptables -F"
  commands << "iptables -t nat -F"
  commands << "iptables -t mangle -F"
  commands << "iptables -P INPUT DROP"
  commands << "iptables -P FORWARD ACCEPT"
  commands << "iptables -P OUTPUT ACCEPT"
  commands << "iptables -A INPUT -p icmp -j ACCEPT"
  commands << "iptables -A INPUT -i lo -j ACCEPT"
  commands << "iptables -A INPUT -p tcp --dport 22 -j ACCEPT"
  commands << "iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT"
  commands << "iptables -A FORWARD -p tcp  --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu"
  commands << "iptables -t nat -A POSTROUTING -o #{gateway_ifname} -j MASQUERADE"
  puts_message "\033[32;1m*\033[0m Setting firewall rules..." unless commands.empty?
  commands.each do |cmd|
    puts_message "  - #{cmd}"
    `#{cmd}`
  end
end

def stop_openvpn
  puts_message "\033[32;1m*\033[0m Stopping OpenVPN..."
  `systemctl stop openvpn`
end

def start_openvpn
  if `ip r | grep default`.chomp.empty?
    puts_message "\033[31;1m*\033[0m Failed to start OpenVPN (no default gateway)."
    return false
  end

  puts_message "\033[32;1m*\033[0m Starting OpenVPN..."
  `systemctl start openvpn`
  counter = 0
  loop do
    break if `ip a | grep tun0`.chomp.size > 0
    counter += 1
    if counter > 20
      puts_message "\033[31;1m*\033[0m Failed to start OpenVPN (timeout 20sec)."
      return false
    end
    sleep 1
  end
  true
end

def set_gateway(ifaces, autoset = false)
  puts_message "\033[32;1m*\033[0m Available gateways:"
  ifaces.size.times do |index|
    puts_message "  #{index}: #{ifaces[index].gateway} (#{ifaces[index].ifname})"
  end
  puts_message
  print_message "\033[33;1m>>\033[0m Gateway ID: "

  if ifaces.empty?
    puts_message "skipped."
    return
  end

  index = 0
  if autoset
    puts_message 0
  else
    index = STDIN.gets.chomp
    if index.empty?
      puts_message "skipped."
      exit
    end
  end

  index = index.to_i
  gateway_iface = ifaces[index]
  gateway_rules = `ip r | grep default`.split("\n")
  gateway_rules.each do |gateway_rule|
    puts_message "  - ip r del #{gateway_rule}"
    `ip r del #{gateway_rule}`
  end

  puts_message "  - ip r add default via #{gateway_iface.gateway} dev #{gateway_iface.ifname}"
  `ip r add default via #{gateway_iface.gateway} dev #{gateway_iface.ifname}`
  puts_message
end

def stop_netmux
  del_routing_rules
  stop_openvpn
  puts_message "netmux stopped."
end

def start_netmux(autoset = false)
  launch_dhclient
  ifaces = get_interfaces(BLACKLIST)
  ifaces.each { |iface| add_routing_rules(iface, ifaces.index(iface) + 1) }
  setup_mptcp(ifaces.size + 1)
  set_gateway(ifaces, autoset)
  if start_openvpn
    set_firewall_rules("tun0")
    puts_message "netmux started."
  end
end

def restart_netmux(autoset = false)
  stop_netmux
  start_netmux(autoset)
end

def launch_dhclient
  ifaces_available = `ip a | grep '<' | awk -F ': ' '{print $2}'`.split("\n").reject{|iface| BLACKLIST.include?(iface)}
  ifaces_with_addr = `ip -4 a | grep '<' | awk -F ': ' '{print $2}'`.split("\n").reject{|iface| BLACKLIST.include?(iface)}
  (ifaces_available - ifaces_with_addr).each do |iface|
    puts_message "  - dhclient #{iface}"
    `dhclient #{iface}`
  end
  sleep 3
end

def server_start
  last_ifaces = `ip a | grep '<' | awk -F ': ' '{print $2}'`.split("\n").reject{|iface| BLACKLIST.include?(iface)}
  puts_message "interface list: #{last_ifaces.empty? ? "(empty)" : last_ifaces.join(", ")}"
  restart_netmux(true)
  state_add_ifaces = false
  state_del_ifaces = false

  loop do
    sleep 5
    ifaces = `ip a | grep '<' | awk -F ': ' '{print $2}'`.split("\n").reject{|iface| BLACKLIST.include?(iface)}

    state_add_ifaces |= (not (ifaces - last_ifaces).empty?)
    state_del_ifaces |= (not (last_ifaces - ifaces).empty?)

    puts_message "interface list: #{ifaces.empty? ? "(empty)" : ifaces.join(", ")}#{state_del_ifaces ? " (degraded)" : ""}" if ifaces != last_ifaces

    if state_add_ifaces or state_del_ifaces

      if state_add_ifaces
        puts_message "found new interface: restarting netmux..."
        restart_netmux(true)
        state_add_ifaces = false
        state_del_ifaces = false
      end
    end
    last_ifaces = ifaces
  end

end

# def assert(a, b)
#   raise "Assert failed: #{a} is not #{b}." unless a == b
# end
# assert(inet2net("192.168.30.150/25"), "192.168.30.128/25")
# assert(suppose_gateway("192.168.42.5/24"), "192.168.42.129")
# assert(suppose_gateway("172.20.10.5/28"), "172.20.10.1")

if ARGV.size != 1
  puts_message "usage: #{$0} {start|stop|restart|server}"
  exit 1
end

case ARGV.first
when "stop"
  stop_netmux
when "start", "restart"
  stop_netmux
  start_netmux(true)
when "server"
  puts_message "log file: /var/log/netmux.log."
  Process.daemon
  $logfile = open("/var/log/netmux.log", "a")
  server_start
else
  puts_message "usage: #{$0} {start|stop|restart|server}"
  exit 1
end

